import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'contacto/:uid',
    loadChildren: () => import('./contact2/contact2.module').then( m => m.Contact2PageModule)
  },
  {
    path: 'confirmacionmensaje',
    loadChildren: () => import('./confirmacionmensaje/confirmacionmensaje.module').then( m => m.ConfirmacionmensajePageModule)
  },
  {
    path: 'lista-contactos',
    loadChildren: () => import('./lista-contactos/lista-contactos.module').then( m => m.ListaContactosPageModule)
  },
  {
    path: 'vehiculos-lista',
    loadChildren: () => import('./pages/vehiculos-lista/vehiculos-lista.module').then( m => m.VehiculosListaPageModule)
  },
  {
    path: 'vehiculos-crear',
    loadChildren: () => import('./pages/vehiculos-crear/vehiculos-crear.module').then( m => m.VehiculosCrearPageModule)
  },
  {
    path: 'localizacion',
    loadChildren: () => import('./localizacion/localizacion.module').then( m => m.LocalizacionPageModule)
  },
  {
    path: 'direcciones',
    loadChildren: () => import('./pages/direcciones/direcciones.module').then( m => m.DireccionesPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
