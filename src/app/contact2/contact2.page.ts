import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Mensaje } from '../model/mensaje';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-contact2',
  templateUrl: './contact2.page.html',
  styleUrls: ['./contact2.page.scss'],
})
export class Contact2Page implements OnInit {

  message: Mensaje = new Mensaje();
  uid: string;      //identifcador del documento

  worked: boolean = false;


  constructor(private route: ActivatedRoute, private router: Router,
    public contactService: ContactosService) { 

    this.uid = this.route.snapshot.paramMap.get('uid');
    console.log("consultando ", this.uid);

    this.contactService.getContactoById2(this.uid).subscribe(data => {
        console.log(data)
        const aux:any = data
        this.message = aux[0];
    });

  }

  ngOnInit() {
  }

  guardar(){
    console.log(this.message, this.worked);

    this.contactService.saveContacto(this.message);
 
    let navigationExtras: NavigationExtras = {
      queryParams: {
        message: this.message,
        worked: this.worked
      }
    };

    this.router.navigate(['/confirmacionmensaje'], navigationExtras);
  }

}
