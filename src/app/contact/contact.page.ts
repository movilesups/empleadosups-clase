import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Mensaje } from '../model/mensaje';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  message: Mensaje = new Mensaje();

  worked: boolean = false;

  imgData: string;
  imgURL: string;


  constructor(private route: ActivatedRoute, private router: Router,
    public contactService: ContactosService) { 

    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (this.router.getCurrentNavigation().extras.queryParams) {
        this.message = this.router.getCurrentNavigation().extras.queryParams.contacto;
        console.log(this.message.image);
      }
    });
  }

  ngOnInit() {
  }

  guardar(){
    console.log(this.message, this.worked);

    this.contactService.saveContacto(this.message);
 
    let navigationExtras: NavigationExtras = {
      queryParams: {
        message: this.message,
        worked: this.worked
      }
    };

    this.router.navigate(['/confirmacionmensaje'], navigationExtras);
  }

  imageSeleccionada(data){
    console.log(data);
    this.imgData = data;
  }

  uploadFinished(data){
    this.message.image = data;
  }

}
