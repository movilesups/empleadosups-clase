import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VehiculosCrearPage } from './vehiculos-crear.page';

const routes: Routes = [
  {
    path: '',
    component: VehiculosCrearPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VehiculosCrearPageRoutingModule {}
