import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VehiculosCrearPageRoutingModule } from './vehiculos-crear-routing.module';

import { VehiculosCrearPage } from './vehiculos-crear.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VehiculosCrearPageRoutingModule
  ],
  declarations: [VehiculosCrearPage]
})
export class VehiculosCrearPageModule {}
