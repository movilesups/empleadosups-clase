import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VehiculosCrearPage } from './vehiculos-crear.page';

describe('VehiculosCrearPage', () => {
  let component: VehiculosCrearPage;
  let fixture: ComponentFixture<VehiculosCrearPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiculosCrearPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VehiculosCrearPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
