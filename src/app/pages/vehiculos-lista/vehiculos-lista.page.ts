import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { VehiculosService } from 'src/app/services/vehiculos.service';

@Component({
  selector: 'app-vehiculos-lista',
  templateUrl: './vehiculos-lista.page.html',
  styleUrls: ['./vehiculos-lista.page.scss'],
})
export class VehiculosListaPage implements OnInit {

  vehiculos: any;

  constructor(public vhService: VehiculosService,
      private router: Router) { }

  ngOnInit() {
    this.vehiculos = this.vhService.getVehiculos();
  }

  editarVehiculo(vehiculo: any){
    console.log(vehiculo);

    
    let navigationExtras: NavigationExtras = {
      queryParams: {
        vehiculo: vehiculo
      }
    };

    this.router.navigate(['/vehiculos-crear'], navigationExtras);
  }

}
