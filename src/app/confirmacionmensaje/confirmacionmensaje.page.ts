import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Mensaje } from '../model/mensaje';

@Component({
  selector: 'app-confirmacionmensaje',
  templateUrl: './confirmacionmensaje.page.html',
  styleUrls: ['./confirmacionmensaje.page.scss'],
})
export class ConfirmacionmensajePage implements OnInit {

  message: Mensaje;
  worked: boolean;

  constructor(private route: ActivatedRoute, private router: Router) { 


    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (this.router.getCurrentNavigation().extras.queryParams) {
        this.message = this.router.getCurrentNavigation().extras.queryParams.message;
        console.log(this.message);
        this.worked = this.router.getCurrentNavigation().extras.queryParams.worked;
        console.log(this.worked)
      }
    });


  }

  ngOnInit() {
  }

}
