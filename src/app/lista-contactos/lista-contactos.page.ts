import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Mensaje } from '../model/mensaje';
import { ContactosService } from '../services/contactos.service';
import { NotificacionesService } from '../services/notificaciones.service';

@Component({
  selector: 'app-lista-contactos',
  templateUrl: './lista-contactos.page.html',
  styleUrls: ['./lista-contactos.page.scss'],
})
export class ListaContactosPage implements OnInit {


  contactos: Observable<any[]>;

  constructor(public contactoService: ContactosService, 
    public router: Router, public notificacionesService: NotificacionesService,
    ) { }

  ngOnInit() {
    this.contactos = this.contactoService.getContactos();
  }

  nuevoContacto(){
    this.router.navigate(['/contacto']);
  }

  editarContacto(contacto: Mensaje){

    let navigationExtras: NavigationExtras = {
      queryParams: {
        contacto: contacto
      }
    };

    this.router.navigate(['/contacto'], navigationExtras);
  }

  editarContactoById(uid: string){
    const url = '/contacto/' + uid;
    console.log(url);
    this.router.navigate([url]); 

  }

  async borrarContacto(uid: string){
    this.contactoService.borrarContacto(uid);
    this.notificacionesService.notificacionToast("Registro borrado");
  }

  async confirmarBorrado(uid: string) {
    this.notificacionesService.confirmacion(
        "Confirmar", 
        "Esta seguro de borrar", 
        this.borrarContacto.bind(this, uid));
  }

}
