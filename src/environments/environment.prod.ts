const API_URL = 'http://ups.edu.ec:8080/parqueadero/rest/';


export const environment = {
  production: true,

  firebase: {
    apiKey: "AIzaSyD1B4qxPil92_ILMK2kTDarvMyI6h1UHyo",
    authDomain: "empleosups-102c9.firebaseapp.com",
    projectId: "empleosups-102c9",
    storageBucket: "empleosups-102c9.appspot.com",
    messagingSenderId: "47171792258",
    appId: "1:47171792258:web:20f743e64f6014290eb39b"
  },

  URL_GET_VEHICULOS: `${API_URL}vehiculo/coleccion`,
  URL_SAVE_VEHICULO: `${API_URL}vehiculo`

};
