// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const API_URL = 'http://localhost:8080/parqueadero/rest/';

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyD1B4qxPil92_ILMK2kTDarvMyI6h1UHyo",
    authDomain: "empleosups-102c9.firebaseapp.com",
    projectId: "empleosups-102c9",
    storageBucket: "empleosups-102c9.appspot.com",
    messagingSenderId: "47171792258",
    appId: "1:47171792258:web:20f743e64f6014290eb39b"
  },

  URL_GET_VEHICULOS: `${API_URL}vehiculo/coleccion`,
  URL_SAVE_VEHICULO: `${API_URL}vehiculo`

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
